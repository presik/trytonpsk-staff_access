# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import itertools
from datetime import datetime, time
from decimal import Decimal

from trytond.i18n import gettext
from trytond.ir.date import Date
from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.report import Report
from trytond.exceptions import UserError
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateTransition, StateView, Wizard
from .exceptions import StaffAccessEnterError


class StaffAccess(Workflow, ModelSQL, ModelView):
    "Staff Access"
    __name__ = 'staff.access'
    STATES = {'readonly': (Eval('state') == 'done')}
    employee = fields.Many2One('company.employee', 'Employee', required=True,
        states=STATES)
    enter_timestamp = fields.DateTime('Enter', required=True, states=STATES)
    exit_timestamp = fields.DateTime('Exit', states=STATES)
    start_rest = fields.DateTime('Start Rest', states=STATES)
    end_rest = fields.DateTime('End Rest', states=STATES)
    rest = fields.Float('Rest', states=STATES, digits=(3, 2))
    state = fields.Selection([
        ('open', 'Open'),
        ('close', 'Close'),
        ('done', 'Done'),
    ], 'State', readonly=True)
    shift = fields.Many2One('staff.shift', 'Shift', states={
        'readonly': True
    }, domain=[
        ('employee', '=', Eval('employee'))
    ])
    event = fields.Many2One('staff.event', 'Event', states=STATES)

    @classmethod
    def __setup__(cls):
        super(StaffAccess, cls).__setup__()
        cls._order.insert(0, ('enter_timestamp', 'ASC'))
        cls._transitions |= set((
            ('open', 'close'),
            ('close', 'done'),
        ))

    @staticmethod
    def default_state():
        return 'open'

    @staticmethod
    def default_enter_timestamp():
        return datetime.now()

    @staticmethod
    def default_rest():
        return 0

    @fields.depends('start_rest', 'end_rest', 'rest')
    def on_change_with_rest(self):
        if self.start_rest and self.end_rest:
            return self.compute_timedelta(self.start_rest, self.end_rest)

    def compute_timedelta(self, start, end):
        res = 0
        if end and start:
            delta = end - start
            res = float(delta.seconds) / 3600
            res = Decimal(str(round(res, 2)))
        return res

    def pre_validate(self):
        super().pre_validate()

        exists = self.__class__.search([
            ('employee', '=', self.employee.id),
            ('enter_timestamp', '=', self.enter_timestamp),
            ('id', '!=', self.id),
        ])

        if exists:
            raise UserError(
                'Registro Duplicado',
                'Ya existe un registro para el empleado %s con fecha de ingreso %s' %
                (self.employee.rec_name, self.enter_timestamp)
            )

    @classmethod
    def validate(cls, records):
        super(StaffAccess, cls).validate(records)
        for record in records:
            record.check_exit_timestamp()

    def check_exit_timestamp(self):
        if self.exit_timestamp:
            timework = self.exit_timestamp - self.enter_timestamp
            if timework.days >= 2 or self.exit_timestamp < self.enter_timestamp:
                raise StaffAccessEnterError(
                    gettext('staff_access.msg_error_exit_timestamp',
                        enter_timestamp=str(self.enter_timestamp),
                        exit_timestamp=str(self.exit_timestamp),
                        employee=self.employee.party.name))

    @classmethod
    def write(cls, access, vals):
        if vals.get("enter_timestamp") or vals.get("exit_timestamp"):
            for acc in access:
                if acc.enter_timestamp and acc.exit_timestamp:
                    acc.write([acc], {'state': 'close'})
                else:
                    acc.write([acc], {'state': 'open'})
        super(StaffAccess, cls).write(access, vals)


class ReportHoursStart(ModelView):
    "Report Hours Start"
    __name__ = 'staff.access_report_hours.start'
    period = fields.Many2One('account.period', 'Period', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class ReportHoursWiz(Wizard):
    "Report Hours"
    __name__ = 'staff.access_report_hours'
    start = StateView('staff.access_report_hours.start',
        'staff_access.report_hours_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('staff.access_hours.report')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'start_date': self.start.period.start_date,
            'end_date': self.start.period.end_date,
            'period_id': self.start.period.id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class ReportHours(Report):
    __name__ = 'staff.access_hours.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        user = pool.get('res.user')(Transaction().user)
        Access = pool.get('staff.access')
        Company = pool.get('company.company')
        company = Company(data['company'])
        AccountPeriod = pool.get('account.period')
        period = AccountPeriod(data['period_id'])
        access = Access.search([
            ('enter_timestamp', '>=', datetime.combine(data['start_date'], datetime.min.time())),
            ('enter_timestamp', '<=', datetime.combine(data['end_date'], datetime.max.time())),
        ])
        lines_dom = []
        party_ids = []
        for line in access:
            party_id = line.employee.party.id

            if party_id in party_ids:
                continue

            employee = Access.search([
                ('enter_timestamp', '>=', datetime.combine(data['start_date'], datetime.min.time())),
                ('enter_timestamp', '<=', datetime.combine(data['end_date'], datetime.max.time())),
                ('employee.party.id', '=', party_id),
            ])
            result = 0
            for e in employee:
                hour_1 = time(21, 0, 0)
                hour_2 = time(22, 0, 0)
                start_time_local = company.convert_timezone(e.enter_timestamp)
                end_time_local = company.convert_timezone(e.exit_timestamp)
                start_work_hour = time(start_time_local.hour, start_time_local.minute, start_time_local.second)
                finish_work_hour = time(end_time_local.hour, end_time_local.minute, end_time_local.second)

                nine_pm = datetime.combine(start_time_local, hour_1)
                ten_pm = datetime.combine(start_time_local, hour_2)
                start_work_date = datetime.combine(start_time_local, start_work_hour)
                end_work_date = datetime.combine(end_time_local, finish_work_hour)

                if start_work_date <= nine_pm and end_work_date >= ten_pm:
                    result += 1
                elif start_work_date >= nine_pm and end_work_date <= ten_pm:
                    hour = str(end_work_date - start_work_date)
                    if hour[2] == ':':
                        s = int(hour[6:8])
                        h = int(hour[3:5])
                    elif hour[1] == ':':
                        s = int(hour[5:7])
                        h = int(hour[2:4])
                    result += h / 60 + s / 3600
                elif start_work_date <= nine_pm and end_work_date <= ten_pm and end_work_date > nine_pm:
                    hour = str(end_work_date - nine_pm)
                    if hour[2] == ':':
                        s = int(hour[6:8])
                        h = int(hour[3:5])
                    elif hour[1] == ':':
                        s = int(hour[5:7])
                        h = int(hour[2:4])
                    result = h / 60 + s / 3600
                elif start_work_date >= nine_pm and start_work_date < ten_pm and end_work_date >= ten_pm:
                    hour = str(ten_pm - start_work_date)
                    if hour[2] == ':':
                        s = int(hour[6:8])
                        h = int(hour[3:5])
                    elif hour[1] == ':':
                        s = int(hour[5:7])
                        h = int(hour[2:4])
                    result = h / 60 + s / 3600
                else:
                    continue

            if result == 0:
                continue
            type_document = ''
            if line.employee.party.type_document:
                if line.employee.party.type_document == '13':
                    type_document = 'Cédula de ciudadanía'
                elif line.employee.party.type_document == '11':
                    type_document = 'Registro Civil de Nacimiento'
                elif line.employee.party.type_document == '12':
                    type_document = 'Tarjeta de Identidad'
                elif line.employee.party.type_document == '21':
                    type_document = 'Tarjeta de Extranjeria'
                elif line.employee.party.type_document == '22':
                    type_document = 'Cedula de Extranjeria'
                elif line.employee.party.type_document == '31':
                    type_document = 'Nit'
                elif line.employee.party.type_document == '41':
                    type_document = 'Pasaporte'
                elif line.employee.party.type_document == '42':
                    type_document = 'Tipo de Documento Extranjero'
                elif line.employee.party.type_document == '43':
                    type_document = 'Sin identificacion del Exterior o para uso definido por la DIAN'
                else:
                    type_document = 'None'

            line.type_document = type_document
            line.hour = result
            party_ids.append(party_id)
            lines_dom.append(line)

        report_context['company'] = user.company.party.name
        report_context['month'] = period.name
        report_context['records'] = lines_dom
        return report_context


class AddAccessGroupStart(ModelView):
    "Add Access group Start"
    __name__ = 'staff.access.add_access_group.start'
    enter_timestamp = fields.DateTime('Enter', required=True)
    exit_timestamp = fields.DateTime('Exit', required=True)
    start_rest = fields.DateTime('Start Rest')
    end_rest = fields.DateTime('End Rest')
    employees = fields.Many2Many('company.employee', None, None,
            'Employees', required=True,
            domain=[('contracting_state', '=', 'active')])
    project = fields.Many2One('project.work', 'Project')


class AddAccessGroup(Wizard):
    "Add Access Group"
    __name__ = 'staff.access.add_access_group'

    start = StateView(
        'staff.access.add_access_group.start',
        'staff_access.add_access_group_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        Access = Pool().get('staff.access')
        enter_ = self.start.enter_timestamp
        exit_ = self.start.exit_timestamp
        enter_rest_ = self.start.start_rest
        exit_rest_ = self.start.end_rest
        project_ = self.start.project

        for e in self.start.employees:
            value = {
                'employee': e.id,
                'enter_timestamp': enter_,
                'exit_timestamp': exit_,
                'start_rest': enter_rest_,
                'end_rest': exit_rest_,
                'rest': Access.compute_timedelta(self, enter_rest_, exit_rest_) if enter_rest_ and exit_rest_ else 0,
                'project': project_,
            }
            Access.create([value])
        return 'end'


class AddAccessFileStart(ModelView):
    "Add Access File Start"
    __name__ = 'staff_access.add_access_file.start'
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    file = fields.Binary(
        'CSV file', help="Load file csv separated comma in utf-8 format",
        required=True, filename='employees_update')


class AddAccessFile(Wizard):
    "Add Access File"
    __name__ = 'staff_access.add_access_file'
    start = StateView('staff_access.add_access_file.start',
        'staff_access.add_access_file_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Update', 'open_', 'tryton-ok', default=True),
        ])

    open_ = StateTransition()

    def transition_open_(self):
        pool = Pool()
        StaffAccess = pool.get('staff.access')
        Employee = pool.get('company.employee')
        csv_file = self.start.file.decode('utf-8')
        reader = csv_file.splitlines()[2:]
        employees = []
        records = sorted([r.split(',') for r in reader], key=lambda record: record[0])
        grouped_data = itertools.groupby(records, key=lambda record: record[0])
        StaffSchedule = Pool().get('staff_access.schedule')
        staff_schedules = StaffSchedule.search_read([
                ('scheduling_date', '>=', self.start.start_date),
                ('scheduling_date', '<=', self.start.end_date),
            ], fields_names=['employee', 'scheduling_date', 'shift_kind'])
        for code, employee_records in grouped_data:
            employee, = Employee.search_read([('party.id_number', '=', code)])
            staff_schedule = [ss for ss in staff_schedules if ss['employee'] == employee['id']]
            employees.extend(self.process_records(employee['id'], list(sorted(employee_records, key=self.sorted_by_date)), staff_schedule))
        StaffAccess.create(employees)
        return 'end'

    def sorted_by_date(self, record):
        return (record[6], record[7])

    def process_records(self, employee, records, staff_schedule):
        result = []
        index = 0
        total_records = len(records)
        while index < total_records - 1:
            shift_kind = None
            _staff_schedule = [ss for ss in staff_schedule
                               if ss['employee'] == employee
                               and str(ss['scheduling_date']) == records[index][6]]
            if _staff_schedule:
                shift_kind = _staff_schedule[0]['shift_kind']
                staff_schedule.remove(_staff_schedule[0])

            access = {
                    'employee': employee,
                    'shift_kind': shift_kind,
                    'enter_timestamp': self.format_date(records[index][6], records[index][7]),
                }
            if index + 2 < total_records and records[index + 2][6] == records[index][6]:
                access['start_rest'] = self.format_date(records[index + 1][6], records[index + 1][7])
                access['end_rest'] = self.format_date(records[index + 2][6], records[index + 2][7])
                access['exit_timestamp'] = self.format_date(records[index + 3][6], records[index + 3][7])
                index += 3
            else:
                access['exit_timestamp'] = self.format_date(records[index + 1][6], records[index + 1][7])
                index += 2
            result.append(access)
        return result

    def format_date(self, _date, _time):
        _date = datetime.strptime(_date, "%Y-%m-%d")
        _time = datetime.strptime(_time, "%H:%M")
        return Date(datetime.combine(_date, _time.time()))
