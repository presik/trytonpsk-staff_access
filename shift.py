from datetime import datetime, time, timedelta

from trytond.model import ModelSQL, ModelView, Workflow, DeactivableMixin, fields
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateTransition, StateView, Wizard

DAYS = [
    (0, 'Monday'),
    (1, 'Tuesday'),
    (2, 'Wednesday'),
    (3, 'Thursday'),
    (4, 'Friday'),
    (5, 'Saturday'),
    (6, 'Sunday'),
]


class StaffZone(ModelSQL, ModelView):
    """Staff Zone"""
    __name__ = 'staff.zone'
    name = fields.Char('Name')


class ShiftKind(ModelSQL, ModelView, DeactivableMixin):
    "Shift Kind"
    __name__ = "staff.shift.kind"
    name = fields.Char('Name', required=True)
    code = fields.Char('Code')
    start = fields.Time('Start', required=True)
    end = fields.Time('End', required=True)
    time_rest = fields.Float('Time Rest', digits=(2, 2))
    total_time = fields.Function(fields.Float('Total Time',
        help='In hours'), 'get_total_time')
    legal_work_time = fields.Float('Legal Work Time', digits=(2, 2))
    mode = fields.Selection([
        ('work', 'Work'),
        ('event', 'Event'),
        ('rest', 'Rest'),
        ('none', 'None'),
    ], 'Mode', required=True)
    event_category = fields.Many2One('staff.event_category', 'Event')
    breakfast_included = fields.Boolean('Breakfast Included')
    lunch_included = fields.Boolean('Lunch Included')
    dinner_included = fields.Boolean('Dinner Included')

    @classmethod
    def __setup__(cls):
        super(ShiftKind, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))

    @staticmethod
    def default_mode():
        return 'work'

    @staticmethod
    def default_time_rest():
        return 0

    def get_total_time(self, name):
        if self.start and self.end:
            t1 = timedelta(hours=self.start.hour, minutes=self.start.minute)
            t2 = timedelta(hours=self.end.hour, minutes=self.end.minute)
            return round((t2 - t1).seconds / 3600, 2)
        return 0


class Shift(Workflow, ModelSQL, ModelView):
    "Staff Shift"
    __name__ = 'staff.shift'
    STATES = {'readonly': Eval('state') != 'draft'}
    employee = fields.Many2One('company.employee', 'Employee', required=True)
    shift_date = fields.Date('Shift Date', required=True, states=STATES)
    shift_kind = fields.Many2One('staff.shift.kind', 'Shift Kind',
        required=False, states=STATES)
    enter_timestamp = fields.DateTime('Enter', states=STATES)
    exit_timestamp = fields.DateTime('Exit', states=STATES)
    section = fields.Many2One('staff.section', 'Section')
    zone = fields.Many2One('staff.zone', 'Zone')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('done', 'Done'),
        ('cancelled', 'Cancelled'),
        ], 'State', readonly=True)
    permission = fields.Selection([
        ('arrive_late', 'Arrive Late'),
        ('early_departure', 'Early Departure'),
        ('', ''),
        ], 'Permission', states=STATES)
    total_time = fields.Function(fields.Float('Total Time'), 'get_total_time')
    notes = fields.Text('Notes', states=STATES)
    attendance = fields.One2Many('staff.access', 'shift', 'Attendance',
        states={'readonly': True})
    # This must be added in another module
    #analytic = fields.Many2One('analytic.account', 'Analytic', required=True)

    @classmethod
    def __setup__(cls):
        super(Shift, cls).__setup__()
        cls._order.insert(0, ('shift_date', 'DESC'))
        cls._transitions |= set((
                ('draft', 'confirmed'),
                ('confirmed', 'draft'),
                ('confirmed', 'done'),
                ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['draft', 'done']),
            },
            'confirm': {
                'invisible': Eval('state').in_(['confirmed', 'done', 'cancelled']),
            },
            'done': {
                'invisible': Eval('state').in_(['draft', 'done']),
            },
            'cancel': {
                'invisible': Eval('state').in_(['cancelled', 'done']),
            },
            # 'add_shift': {
            #     'invisible': Eval('state') != 'draft',
            # },
        })

    def get_rec_name(self, name):
        return '[' + str(self.shift_date) + '] ' + self.employee.rec_name

    def get_total_time(self, name=None):
        if self.exit_timestamp and self.enter_timestamp:
            return (self.exit_timestamp - self.enter_timestamp).total_seconds() / 3600

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirm(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, records):
        pass

    @classmethod
    def create_staff_access(cls, records):
        StaffAccess = Pool().get('staff.access')
        shifts = []
        for record in records:
            shift_ = {
                'employee': record.employee,
                'enter_timestamp': record.enter_timestamp,
                'exit_timestamp': record.exit_timestamp,
                'start_rest': record.start_rest,
                'end_rest': record.end_rest,
                'state': 'open',
                'payment_method': 'extratime',
            }
            shifts.append(shift_)
        StaffAccess.create(shifts)

    @fields.depends('shift_kind', 'enter_timestamp', 'exit_timestamp')
    def on_change_shift_kind(self):
        if self.shift_kind:
            enter_tz = time(
                    self.shift_kind.start.hour + 5,
                    self.shift_kind.start.minute)
            self.enter_timestamp = datetime.combine(self.shift_date, enter_tz)
            self.exit_timestamp = self.enter_timestamp + timedelta(seconds=self.shift_kind.total_time * 3600)
        else:
            self.enter_timestamp = None
            self.exit_timestamp = None

    @classmethod
    def get_shift_schedule(cls, args):
        RANGE = 20
        oc = args.get('oc')
        section = args.get('section')
        start_date = args.get('start_date')
        end_date =  start_date + timedelta(days=20)
        cls.search_read([
            ('shift_date', '>=', date_),
            ('shift_date', '<=', end_date)
        ])
        pass


class StaffSection(ModelSQL, ModelView):
    "Staff Section"
    __name__ = 'staff.section'
    name = fields.Char('Name', required=True)
    code = fields.Char('Code')
    chief = fields.Many2One('company.employee', 'Chief', required=False)
    # We need add operation center here
    employees = fields.Many2Many('staff.section-company.employee',
            'section', 'employee', 'Employees')


class SectionEmployee(ModelSQL):
    "Section - Employee"
    __name__ = 'staff.section-company.employee'
    section = fields.Many2One('staff.section', 'Section', required=True,
        ondelete='CASCADE')
    employee = fields.Many2One('company.employee', 'Employee',
            required=True, ondelete='CASCADE')


class AddShiftStart(ModelView):
    "Add Shift Start"
    __name__ = 'staff.add_shift.start'
    position = fields.Many2One('shift_access.position', 'Position')
    shift_kind = fields.Many2One('staff.shift.kind', 'Shift Kind')
    employees = fields.Many2Many('company.employee', None, None,
            'Employees', required=True)
    enter_hour = fields.Time('Enter Hour', required=True)
    timework = fields.Float('Time Work', required=True)

    @fields.depends('shift_kind', 'enter_hour', 'timework')
    def on_change_shift_kind(self):
        if self.shift_kind:
            self.enter_hour = self.shift_kind.start
            self.timework = self.shift_kind.total_time
        else:
            self.enter_hour = None
            self.timework = None


class AddShift(Wizard):
    "Add Shift"
    __name__ = 'staff.add_shift'
    start = StateView(
        'staff.add_shift.start',
        'staff_access.add_shift_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def convert_seconds(self, value):
        hour = int(value)
        minute = (float(value) - int(value)) * 3600
        return hour, minute

    def transition_accept(self):
        ids = Transaction().context['active_ids']
        Shift = Pool().get('staff.shift')
        if ids:
            shift = Shift(ids[0])
            lines_to_create = []
            enter_hour_tz = time(
                    self.start.enter_hour.hour + 5,
                    self.start.enter_hour.minute)
            enter_ = datetime.combine(shift.shift_date, enter_hour_tz)
            exit_ = enter_ + timedelta(0, self.start.timework * 3600)
            for e in self.start.employees:
                lines_to_create.append({
                    'employee': e.id,
                    'position': self.start.position if self.start.position else None,
                    'enter_timestamp': enter_,
                    'exit_timestamp': exit_,
                    'state': 'draft',
                })

            Shift.write([shift], {
                'lines': [('create', lines_to_create)],
                },
            )
        return 'end'


class ShiftSheetReport(Report):
    "Shift Sheet Report"
    __name__ = 'staff_access.shift_sheet_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        Company = Pool().get('company.company')
        report_context['company'] = Company(Transaction().context['company'])
        return report_context


class SelectShiftAsk(ModelView):
    "Select Product Ask"
    __name__ = 'staff_access.select_shift_ask'
    scheduling_by_period = fields.Many2One('staff_access.create_scheduling_by_period.start', 'Scheduling By Period')
    shift_line = fields.Many2One('staff.shift.line', 'Shift Line')
    day = fields.Selection(DAYS, 'Day', states={'readonly': True})
    enter_timestamp = fields.DateTime('Enter', required=True)
    exit_timestamp = fields.DateTime('Exit', required=True)
    start_rest = fields.DateTime('Enter Rest')
    end_rest = fields.DateTime('Exit Rest')
    position = fields.Many2One('shift_access.position', 'position')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('done', 'Done'),
        ('canceled', 'Canceled'),
    ], 'State', states={'readonly': True})
    ignore = fields.Boolean('Ignore')
    total_hours_work = fields.Function(fields.Numeric('Total Hours Work'), 'get_total_time')

    def get_total_time(self, name=None):
        if self.enter_timestamp and self.exit_timestamp:
            t1 = timedelta(hours=self.enter_timestamp.hour, minutes=self.enter_timestamp.minute)
            t2 = timedelta(hours=self.exit_timestamp.hour, minutes=self.exit_timestamp.minute)
            return round((t2 - t1).seconds / 3600, 2)
        return 0

    @staticmethod
    def default_scheduling_by_period():
        return 1


class CreateShiftSchedulingStart(ModelView):
    'Create Shift Scheduling Start'
    __name__ = 'staff_access.create_shift_scheduling.start'
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('Start Date', required=True)
    employees = fields.Many2Many('company.employee', None, None, 'Employees',
        required=True)
    section = fields.Many2One('staff.section', 'Section',)


class CreateShiftScheduling(Wizard):
    'Create Shift Scheduling'
    __name__ = 'staff_access.create_shift_scheduling'
    start = StateView(
        'staff_access.create_shift_scheduling.start',
        'staff_access.create_shift_scheduling_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create Shift', 'create_shift', 'tryton-ok', default=True),
        ])
    create_shift = StateTransition()

    def transition_create_shift(self):
            period = self.start.period
            shift_ask = self.start.shift_ask
            StaffSchedule = Pool().get('staff_access.schedule')
            schedule_days = []
            for_delete = []
            for schedule in shift_ask:
                employee = schedule.employee
                year = period.start_date.year
                month = period.start_date.month
                for day in range(1, 32):
                    current_day = getattr(schedule, f"day_{day}")
                    if current_day:
                        current_day.employee = employee.id
                        current_day.scheduling_date = date(year, month, day)
                        schedule_days.append(current_day)
                        if current_day.state == 'for_delete':
                            for_delete.append(current_day)
            StaffSchedule.save(schedule_days)
            StaffSchedule.delete(for_delete)
            return 'end'
