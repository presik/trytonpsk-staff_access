# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import datetime, timedelta, time, date

from trytond.model import DeactivableMixin, Workflow, ModelView, ModelSQL, fields
from trytond.wizard import (
    Wizard, StateTransition, StateReport, StateView, Button)
from trytond.pyson import Eval, If
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.report import Report
from datetime import date, timedelta
from trytond.modules.account.exceptions import (CancelWarning)
from itertools import chain
import polars as pol
from .queries import REPORT_SCHEDULED_ACCESS_EXECUTED

STATES = {
    'required': True,
    'readonly': Eval('state') != 'draft'
}
DOMAIN_SCHEDULE = [
    ('id', '=', -1),
    ]

DAYS = ['day_'+str(day) for day in range(1, 32)]


class StaffSchedule(Workflow, ModelSQL, ModelView):
    'Staff Schedule'
    __name__ = 'staff_access.schedule'
    scheduling_date = fields.Date('Scheduling Date')
    employee = fields.Many2One('company.employee', 'Employee', STATES)
    section = fields.Many2One('staff.section', 'Section', STATES)
    shift_kind = fields.Many2One('staff.shift.kind', 'Shift Kind', states={'required': True})
    state = fields.Selection([
        ('draft', 'Draft'),
        ('for_delete', 'Delete'),
        ('confirmed', 'Confirmed'),
        ('done', 'Done'),
        ('canceled', 'Canceled'),
    ], 'State')
    cancellation_text = fields.Char('Cancellation Text',
                                    states={'required': Eval('state') == 'canceled'})
    cancellation_reason = fields.Selection([
        (None, ''),
        ('justified', 'Justified'),
        ('unjustified', 'Unjustified')
    ], 'Cancellation Reason', states={'required': Eval('state') == 'canceled'})

    @classmethod
    def __setup__(cls):
        super(StaffSchedule, cls).__setup__()
        cls._transitions |= set((
                ('draft', 'confirmed'),
                ('draft', 'for_delete'),
                ('confirmed', 'draft'),
                ('confirmed', 'canceled'),
                ('canceled', 'draft'),
                ('confirmed', 'done'),
                ))
        cls._buttons.update({
                'done': {
                    'invisible': Eval('state') != 'confirmed',
                    },
                'confirmed': {
                    'invisible': Eval('state').in_(['confirmed', 'done']),
                    },
                'canceled': {
                        'invisible': Eval('state').in_(['canceled', 'done', 'draft']),
                    },
                'draft': {
                        'invisible': Eval('state').in_(['canceled', 'draft', 'done']),
                    },
                'for_delete': {
                    'invisible': Eval('state') != 'draft',
                }
        })

    @classmethod
    def view_attributes(cls):
        return super().view_attributes() + [
            ('/tree/field[@name="state"]',
                'visual', If(Eval('state') == 'canceled', 'danger', ''))
        ]

    @staticmethod
    def default_state():
        return 'draft'

    def get_rec_name(self, name):
        # code = self.analytic.code if self.analytic.code else self.analytic.name
        rec_name = ''
        if self.shift_kind and self.section:
            section = self.section.code or self.section.name
            shift_kind = self.shift_kind.code or self.shift_kind.name
            rec_name = section + '/' + shift_kind
            if self.state == 'canceled':
                rec_name = 'Cancelado'
            return rec_name

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('for_delete')
    def for_delete(cls, records):
        print('si intento eliminar')

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirmed(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        cls.create_staff_access(records)

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    def canceled(cls, records):
        pass

    @classmethod
    def create_staff_access(cls, records):
        StaffAccess = Pool().get('staff.access')
        shift_lines = []
        for record in records:
            scheduling_date = record.scheduling_date
            scheduling_date_exit = scheduling_date
            shift_kind = record.shift_kind
            start_rest = None
            end_rest = None
            if shift_kind.start > shift_kind.end:
                scheduling_date_exit = scheduling_date + timedelta(days=1)
            if shift_kind.start_rest and shift_kind.end_rest:
                start_rest = datetime.combine(scheduling_date, shift_kind.start_rest)
                end_rest = datetime.combine(scheduling_date, shift_kind.end_rest)
            shift_line = {
                'employee': record.employee,
                'shift_kind': shift_kind,
                'enter_timestamp': datetime.combine(scheduling_date, shift_kind.start) + timedelta(hours=5),
                'exit_timestamp': datetime.combine(scheduling_date_exit, shift_kind.end) + timedelta(hours=5),
                'start_rest': start_rest,
                'end_rest': end_rest,
                'state': 'open',
                'payment_method': 'extratime'
            }
            shift_lines.append(shift_line)
        StaffAccess.create(shift_lines)


class CreateSchedulingByPeriodStart(ModelView):
    'Create Scheduling By Period Start'
    __name__ = 'staff_access.create_scheduling_by_period.start'
    period = fields.Many2One('account.period', 'Period',
                             states={'required': True})
    department = fields.Many2One('company.department', 'Department',
                             states={'required': True,
                                     'readonly': ~Eval('period')})
    shift_ask = fields.One2Many('staff_access.select_shift_ask',
        'scheduling_by_period', 'Shift Ask')

    @fields.depends('department', 'period', 'shift_ask')
    def on_change_department(self):
        if self.department and self.period:
            pool = Pool()
            Employee = pool.get('company.employee')
            employees = Employee.search([('department', '=', self.department.id),
                                         ('active', '!=', False)])
            ShiftAsk = pool.get('staff_access.select_shift_ask')
            StaffSchedule = pool.get('staff_access.schedule')
            shift_asks = []
            domain = [('scheduling_date', '>=', self.period.start_date),
                      ('scheduling_date', '<=', self.period.end_date)]
            for employee in employees:
                shift_ask = ShiftAsk(employee=employee.id)
                new_domain = domain + [('employee', '=', employee.id)]
                staff_schedules = StaffSchedule.search(new_domain)

                for staff_schedule in staff_schedules:
                    day = staff_schedule.scheduling_date.day
                    setattr(shift_ask, f"day_{day}", staff_schedule)
                    shift_ask.on_change_day_1()
                shift_asks.append(shift_ask)
            self.shift_ask = shift_asks


class SelectShiftAsk(ModelView):
    'Select Product Ask'
    __name__ = 'staff_access.select_shift_ask'
    scheduling_by_period = fields.Many2One('staff_access.create_scheduling_by_period.start', 'Scheduling By Period')
    employee = fields.Many2One('company.employee', 'Employee', readonly=True)
    day_1 = fields.Many2One('staff_access.schedule', 'Day 1', domain=DOMAIN_SCHEDULE)
    day_2 = fields.Many2One('staff_access.schedule', 'Day 2', domain=DOMAIN_SCHEDULE)
    day_3 = fields.Many2One('staff_access.schedule', 'Day 3', domain=DOMAIN_SCHEDULE)
    day_4 = fields.Many2One('staff_access.schedule', 'Day 4', domain=DOMAIN_SCHEDULE)
    day_5 = fields.Many2One('staff_access.schedule', 'Day 5', domain=DOMAIN_SCHEDULE)
    day_6 = fields.Many2One('staff_access.schedule', 'Day 6', domain=DOMAIN_SCHEDULE)
    day_7 = fields.Many2One('staff_access.schedule', 'Day 7', domain=DOMAIN_SCHEDULE)
    day_8 = fields.Many2One('staff_access.schedule', 'Day 8', domain=DOMAIN_SCHEDULE)
    day_9 = fields.Many2One('staff_access.schedule', 'Day 9', domain=DOMAIN_SCHEDULE)
    day_10 = fields.Many2One('staff_access.schedule', 'Day 10', domain=DOMAIN_SCHEDULE)
    day_11 = fields.Many2One('staff_access.schedule', 'Day 11', domain=DOMAIN_SCHEDULE)
    day_12 = fields.Many2One('staff_access.schedule', 'Day 12', domain=DOMAIN_SCHEDULE)
    day_13 = fields.Many2One('staff_access.schedule', 'Day 13', domain=DOMAIN_SCHEDULE)
    day_14 = fields.Many2One('staff_access.schedule', 'Day 14', domain=DOMAIN_SCHEDULE)
    day_15 = fields.Many2One('staff_access.schedule', 'Day 15', domain=DOMAIN_SCHEDULE)
    day_16 = fields.Many2One('staff_access.schedule', 'Day 16', domain=DOMAIN_SCHEDULE)
    day_17 = fields.Many2One('staff_access.schedule', 'Day 17', domain=DOMAIN_SCHEDULE)
    day_18 = fields.Many2One('staff_access.schedule', 'Day 18', domain=DOMAIN_SCHEDULE)
    day_19 = fields.Many2One('staff_access.schedule', 'Day 19', domain=DOMAIN_SCHEDULE)
    day_20 = fields.Many2One('staff_access.schedule', 'Day 20', domain=DOMAIN_SCHEDULE)
    day_21 = fields.Many2One('staff_access.schedule', 'Day 21', domain=DOMAIN_SCHEDULE)
    day_22 = fields.Many2One('staff_access.schedule', 'Day 22', domain=DOMAIN_SCHEDULE)
    day_23 = fields.Many2One('staff_access.schedule', 'Day 23', domain=DOMAIN_SCHEDULE)
    day_24 = fields.Many2One('staff_access.schedule', 'Day 24', domain=DOMAIN_SCHEDULE)
    day_25 = fields.Many2One('staff_access.schedule', 'Day 25', domain=DOMAIN_SCHEDULE)
    day_26 = fields.Many2One('staff_access.schedule', 'Day 26', domain=DOMAIN_SCHEDULE)
    day_27 = fields.Many2One('staff_access.schedule', 'Day 27', domain=DOMAIN_SCHEDULE)
    day_28 = fields.Many2One('staff_access.schedule', 'Day 28', domain=DOMAIN_SCHEDULE)
    day_29 = fields.Many2One('staff_access.schedule', 'Day 29', domain=DOMAIN_SCHEDULE)
    day_30 = fields.Many2One('staff_access.schedule', 'Day 30', domain=DOMAIN_SCHEDULE)
    day_31 = fields.Many2One('staff_access.schedule', 'Day 31', domain=DOMAIN_SCHEDULE)

    total_hours_work = fields.Float('Total Hours Work',
                                    states={'readonly': True})

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_1(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_2(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_3(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_4(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_5(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_6(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_7(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_8(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_9(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_10(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_11(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_12(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_13(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_14(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_15(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_16(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_17(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_18(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_19(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_20(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_21(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_22(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_23(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_24(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_25(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_26(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_27(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_28(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_29(self):
        self.get_total_hours_work()

    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_30(self):
        self.get_total_hours_work()

    @fields.depends('total_hours_work')
    @fields.depends(*DAYS, methods=['get_total_hours_work'])
    def on_change_day_31(self):
        self.get_total_hours_work()

    def get_total_hours_work(self):
        days = 32
        total = 0
        for day in range(1, days):
            time_schedule = getattr(self, f"day_{day}")
            if time_schedule and time_schedule.state != 'canceled':
                total += time_schedule.shift_kind.total_time
        self.total_hours_work = total


# class StaffScheduleAsk(ModelView):
#     'Staff Schedule Ask'
#     __name__ = 'staff_access.schedule_ask.start'
#     # analytic = fields.Many2One('analytic_account.account', 'Analytic Account',
#     #                            domain=[('type', '=', 'normal')])
#     section = fields.Many2One('staff.section', 'Section')
#     shift_kind = fields.Many2One('staff.shift.kind', 'Shift Kind')
#     state = fields.Selection([
#         ('draft', 'Draft'),
#         ('confirmed', 'Confirmed'),
#         ('done', 'Done'),
#         ('canceled', 'Canceled'),
#     ], 'State')


class CreateSchedulingByPeriod(Wizard):
    'Create Scheduling By Period'
    __name__ = 'staff_access.create_scheduling_by_period'
    start = StateView(
        'staff_access.create_scheduling_by_period.start',
        'staff_access.create_scheduling_by_period_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create Shift', 'create_shift', 'tryton-ok', default=True),
            ])
    create_shift = StateTransition()

    def transition_create_shift(self):
        period = self.start.period
        shift_ask = self.start.shift_ask
        StaffSchedule = Pool().get('staff_access.schedule')
        schedule_days = []
        for_delete = []
        for schedule in shift_ask:
            employee = schedule.employee
            year = period.start_date.year
            month = period.start_date.month
            for day in range(1, 32):
                current_day = getattr(schedule, f"day_{day}")
                if current_day:
                    current_day.employee = employee.id
                    current_day.scheduling_date = date(year, month, day)
                    schedule_days.append(current_day)
                    if current_day.state == 'for_delete':
                        for_delete.append(current_day)
        StaffSchedule.save(schedule_days)
        StaffSchedule.delete(for_delete)
        return 'end'


class SchedulingByReportStart(ModelView):
    'scheduling Report Start'
    __name__ = 'staff_access.scheduling_report.start'
    period = fields.Many2One('account.period', 'Period',
                             states={'required': True})


class schedulingReportWizard(Wizard):
    'scheduling Report Wizard'
    __name__ = 'staff_access.scheduling_report'
    start = StateView('staff_access.scheduling_report.start',
        'staff_access.scheduling_report_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'print_', 'tryton-ok', default=True),
            ])

    print_ = StateReport('staff_access.scheduling_report.report')

    def do_print_(self, action):
        data = {
            'start_date': self.start.period.start_date,
            'end_date': self.start.period.end_date,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class schedulingReport(Report):
    "scheduling Report"
    __name__ = 'staff_access.scheduling_report.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Schedule = pool.get('staff_access.schedule')
        last_day = data['end_date'].day
        schedules = Schedule.search([
            ('scheduling_date', '>=', data['start_date']),
            ('scheduling_date', '<=', data['end_date']),
            ('employee', '!=', None)])
        states = {
            'draft': 'Borrador',
            'confirmed': 'Confirmado',
            'done': 'Terminado',
            'canceled': 'Cancelado',
        }
        employees = {}
        for schedule in schedules:
            employee = schedule.employee.party
            section = schedule.section.name if schedule.section else ''
            customer = schedule.section.analytic.name if schedule.section else ''
            try:
                employees[employee.id_number][schedule.scheduling_date.day] = {
                    'customer': customer,
                    'state': schedule.state,
                    'section': section or '',
                    'hours': schedule.shift_kind.total_time
                }
                employees[employee.id_number]['data']['total'] += schedule.shift_kind.total_time
            except KeyError:
                employees[employee.id_number] = {i: None for i in range(1, 32)}
                employees[employee.id_number][schedule.scheduling_date.day] = {
                    'customer': customer,
                    'state': states[schedule.state],
                    'section': section,
                    'hours': schedule.shift_kind.total_time
                }
                employees[employee.id_number]['data'] = {
                    'id_number': schedule.employee.party.id_number,
                    'name': (employee.first_name or '') + ' ' + ' ' + (employee.second_name or ''),
                    'last_name': (employee.first_family_name or '') + ' ' + ' ' + (employee.second_family_name or ''),
                    'total': schedule.shift_kind.total_time
                }
        report_context['employees'] = list(employees.items())
        report_context['last_day'] = last_day
        report_context['month_weekdays'] = cls.get_month_weekdays(data['start_date'], last_day)
        return report_context

    @classmethod
    def get_month_weekdays(cls, my_date, last_day):
        weekday_list = []
        for day in range(0, last_day + 1):
            current_date = my_date + timedelta(days=day)
            weekday_number = current_date.weekday()
            if weekday_number == 0:
                weekday_list.append("Lunes")
            elif weekday_number == 1:
                weekday_list.append("Martes")
            elif weekday_number == 2:
                weekday_list.append("Miercoles")
            elif weekday_number == 3:
                weekday_list.append("Jueves")
            elif weekday_number == 4:
                weekday_list.append("Viernes")
            elif weekday_number == 5:
                weekday_list.append("Sabado")
            else:
                weekday_list.append("Domingo")
        return weekday_list


#################################################################
class ExecuteShiftScheduleStart(ModelView):
    'execute shift schedule'
    __name__ = 'staff_access.execute_shift_schedule.start'
    period = fields.Many2One('account.period', 'Period',
                             states={'required': True})


class ExecuteShiftSchedule(Wizard):
    'Create Scheduling By Period'
    __name__ = 'staff_access.execute_shift_schedule'
    start = StateView(
        'staff_access.execute_shift_schedule.start',
        'staff_access.execute_shift_schedule_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Execute Schedule', 'execute_schedule', 'tryton-ok', default=True),
            ])
    execute_schedule = StateTransition()

    def transition_execute_schedule(self):
        period = self.start.period
        StaffSchedule = Pool().get('staff_access.schedule')
        dom = [
                ('state', 'in', ['draft', 'confirmed']),
                ('shift_kind', '!=', None),
                ('employee', '!=', None),
                ('scheduling_date', '>=', period.start_date),
                ('scheduling_date', '<=', period.end_date),
            ]
        staffSchedules = StaffSchedule.search(dom)
        StaffSchedule.create_staff_access(staffSchedules)
        return 'end'


class ScheduledAccessExecutedStart(ModelView):
    "Scheduled Access Executed Start"
    __name__ = 'staff_access.scheduled_access_executed.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class ScheduledAccessExecuted(Wizard):
    "Scheduled Access Executed"
    __name__ = 'staff_access.scheduled_access_executed'
    start = StateView(
        'staff_access.scheduled_access_executed.start',
        'staff_access.scheduled_access_executed_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateReport('staff_access.scheduled_access_executed.report')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class ScheduledAccessExecutedReport(Report):
    """Balance Programming Executed Report"""
    __name__ = 'staff_access.scheduled_access_executed.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        cursor = Transaction().connection.cursor()
        _query = REPORT_SCHEDULED_ACCESS_EXECUTED.format(
            start_date=data['start_date'], end_date=data['end_date'])
        cursor.execute(_query)
        result = list(map(cls.set_time, cursor.fetchall()))
        # result_df = pol.read_database(_query, cursor)
        for r in result:
            print(r)
        report_context['records'] = result
        return report_context

    @classmethod
    def set_time(cls, row):
        enter_timestamp = cls._format_date(row[7])
        start_rest = cls._format_date(row[8]) if row[7] < row[8] else row[8].time()
        end_rest = cls._format_date(row[9]) if row[10] < row[9] else row[9].time()
        exit_timestamp = cls._format_date(row[10])
        new_dict = {
            'employee': row[0],
            'shift_kind': row[1],
            'scheduling_date': row[2],
            'ssk_start': str(row[3]),
            'ssk_start_rest': str(row[4]),
            'ssk_end_rest': str(row[5]),
            'ssk_end': str(row[6]),
            'enter_timestamp': str(enter_timestamp),
            'enter_timestamp_state': cls.calculate_difference(row[3], enter_timestamp),
            'start_rest': str(start_rest),
            'start_rest_state': cls.calculate_difference(row[4], start_rest),
            'end_rest': str(exit_timestamp),
            'end_rest_state': cls.calculate_difference(row[5], end_rest),
            'exit_timestamp': str(exit_timestamp),
            'exit_timestamp_state': cls.calculate_difference(row[6], exit_timestamp)
        }
        return new_dict

    @classmethod
    def _format_date(cls, _date):
        new_date = _date - timedelta(hours=5) if _date else None
        return new_date.time() if new_date else None

    @classmethod
    def calculate_difference(cls, time_schedule, time_execute):
        if time_schedule and time_execute:
            if time_schedule > time_execute:
                return 'Anticipado' 
            if time_schedule < time_execute:
                return 'Retraso'
            else:
                return 'ok'