REPORT_SCHEDULED_ACCESS_EXECUTED = """
SELECT  pp.name, ssk.name, sas.scheduling_date,
ssk.start, ssk.start_rest, ssk.end_rest, ssk.end,
sa.enter_timestamp, sa.start_rest,
sa.end_rest, sa.exit_timestamp
FROM staff_access_schedule AS sas
JOIN staff_shift_kind AS ssk ON ssk.id = sas.shift_kind
JOIN staff_access AS sa ON sa.employee = sas.employee
AND sa.shift_kind = sas.shift_kind
JOIN company_employee AS ce ON ce.id = sas.employee
JOIN party_party AS pp ON pp.id = ce.party
WHERE sas.scheduling_date >= '{start_date}'
AND sas.scheduling_date <= '{end_date}'
AND sas.scheduling_date = DATE(sa.enter_timestamp)"""
