from trytond.pool import PoolMeta
from trytond.model import ModelSQL, fields

class PositionSection(ModelSQL):
    "Position - Section"
    __name__ = 'staff.position-staff.section'
    position = fields.Many2One('staff.position', 'Position',
            required=True, ondelete='CASCADE')
    section = fields.Many2One('staff.section', 'Section', required=True,
        ondelete='CASCADE')


class Position(metaclass=PoolMeta):
    __name__ = 'staff.position'
    sections = fields.Many2Many('staff.position-staff.section', 'position',
        'section', 'Sections')
