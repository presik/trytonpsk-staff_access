
from trytond.pool import Pool

from . import access
from . import dash
from . import schedule
from . import shift
from . import position


def register():
    Pool.register(
        shift.StaffSection,
        position.PositionSection,
        position.Position,
        shift.Shift,
        shift.ShiftKind,
        access.StaffAccess,
        shift.SectionEmployee,
        shift.AddShiftStart,
        shift.StaffZone,
        access.ReportHoursStart,
        access.AddAccessGroupStart,
        access.AddAccessFileStart,
        dash.DashApp,
        dash.AppAttendanceControl,
        schedule.SchedulingByReportStart,
        schedule.CreateSchedulingByPeriodStart,
        schedule.SelectShiftAsk,
        schedule.StaffSchedule,
        schedule.ScheduledAccessExecutedStart,
        module='staff_access', type_='model')
    Pool.register(
        shift.AddShift,
        access.ReportHoursWiz,
        access.AddAccessGroup,
        access.AddAccessFile,
        schedule.schedulingReportWizard,
        schedule.CreateSchedulingByPeriod,
        schedule.ScheduledAccessExecuted,
        module='staff_access', type_='wizard')
    Pool.register(
        access.ReportHours,
        shift.ShiftSheetReport,
        schedule.schedulingReport,
        schedule.ScheduledAccessExecutedReport,
        module='staff_access', type_='report')
